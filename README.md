### :handshake: ¡Bienvenidos! Soy JuanLu Garrancho 
----

:chart_with_upwards_trend: Analista de datos y profesor de estadística aplicada.

:tools: Los **lenguajes** y **herramientas** que domino son: **R**, **SQL** y **Excel**. 

:seedling: En la actualidad me estoy formando en **Tableau** y **Python**.
